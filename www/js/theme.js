/**
 * Created by aleksandr on 04.04.15.
 */


jQuery(document).ready(function(){

resize_header();

jQuery(window).resize(function(){
    resize_header();
});

jQuery('.blog_item').hover(function(){
        jQuery(this).addClass('top_article');
    },
    function(){
        jQuery(this).removeClass('top_article');
    });

    jQuery('.team_item').hover(function(){
        jQuery(this).find('img').removeClass('filter');
    },
    function(){
        jQuery(this).find('img').addClass('filter');
    }
    );
});


function resize_header(){
    var main_header = jQuery('.main_header');

    if(main_header.length){
        if(main_header.height() < (jQuery(window).height()-280)) {
            main_header.css({
                'height': (jQuery(window).height()-280) + 'px'
            });
            jQuery('.slogan_container').css({
                'position': 'absolute',
                'width': '100%',
                'bottom': '0px',
                'left': 'auto',
                'right': 'auto'
            });
        }
        else {
            main_header.css({
                'height': 'auto'
            });
            jQuery('.slogan_container').css({
                'position': 'relative'
            });
        }

    }
}

jQuery(document).ready(function(){
    jQuery('.economic-consult').hide();
    jQuery('.arbitr-services-list').hide();
    jQuery('.arbitr-question-form').hide();
    jQuery('.price-group-list-collapse').hide();
    jQuery('.nasled-faq__answer-text').hide();
});

jQuery('.economic-question-text').click(function() {
    jQuery(this).parent().parent().find('.economic-consult').toggle(300);
});
jQuery('.arbitr-services-link').click(function() {
    jQuery(this).toggleClass('arbitr-services-open');
    jQuery(this).parent().parent().find('.arbitr-services-list').toggle(300);
});
jQuery('.question-btn button').click(function() {
    jQuery('.arbitr-question-form').show(300);
    jQuery('.question-btn button').toggle(200);
});
jQuery('.price-group-title span').click(function() {
    jQuery(this).toggleClass('price-group-title-open');
    jQuery(this).parent().parent().find('.price-group-list-collapse').toggle(300);
});
jQuery('.nasled-faq__answer-link span').click(function() {
    jQuery(this).parent().toggleClass('nasled-faq__answer-link_open');
    jQuery(this).parent().parent().find('.nasled-faq__answer-text').toggle(300);
});

if (screen.width < '768') {
    /*$(window).scroll(function () {
        $('.logo-new').fadeOut(100);
        $('body').css('padding-top', '68px');
    });*/
    jQuery(function(logoFade){
        var element = logoFade('.logo-new');
        var pageHeight = $(document).height();
        logoFade(window).scroll(function(){
            element['fade'+ (logoFade(this).scrollTop() > pageHeight ? 'In': 'Out')](500);
        });
    });
}

