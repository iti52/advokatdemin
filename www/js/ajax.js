/**
 * Created by aleksandr on 29.11.14.
 */

function result_request(data){
    for (key in data){
        if (key == 'alert'){

        }else{
            if(data[key]['method'] == 'append'){
                jQuery(key).append(data[key]['data']);
            }else if(data[key]['method'] == 'replace'){
                jQuery(key).html(data[key]['data']);
            }else if(data[key]['method'] == 'remove'){
                jQuery(key).remove();
            }
            else{

            }
            if(data[key]['removeclass']){
                jQuery(key).removeClass(data[key]['removeclass']);
            }
        }
    }
    for (key in data){
            if(data[key]['class']){
                jQuery(key).addClass(data[key]['class']);
            }
            if(data[key]['attr']){
                for (key_attr in data[key]['attr']){
                    var opt = {};
                    opt[key_attr] = data[key]['attr'][key_attr];
                    jQuery(key).attr(opt);
                }
            }
    }
}

function update_counts(content_type, pk, cart_pk){
    var quantity = jQuery('#quantity_'+cart_pk).val();
    var csrfmiddlewaretoken = jQuery('input[name=csrfmiddlewaretoken]').val();
    var url = '/cart/'
    jQuery.ajax({
        url: url,
        type: "POST",
        data: 'csrfmiddlewaretoken='+csrfmiddlewaretoken+'&content_type='+content_type+'&object_id='+pk+'&quantity='+quantity,
        success: function(response) {
            result_request(response);
        },
        error: function(response) {
            alert(response);
        }
    });
}

function send_form(form_selector, url){
    var form_data = jQuery(form_selector).serialize();
    jQuery.ajax({
        url: url,
        type: "POST",
        data: form_data,
        success: function(response) {
            result_request(response);
        },
        error: function(response) {
            alert(response);
        }
    });
}

function get_ajax_data(url){
    jQuery.ajax({
        url: url,
        type: "GET",
        data: 'pk_f=12',
        success: function(response) {
            result_request(response);
        },
        error: function(response) {
            console.log(response);
        }
    });
}

function generate_map(selector_block, center, placemarks){
    ymaps.ready(init);
    var myMap,
        myPlacemark;

    function init(){
        myMap = new ymaps.Map(selector_block, {
            center: center,
            zoom: 13,
            width: '100%'
        });
        placemarks.forEach(function(entry) {
            myPlacemark = new ymaps.Placemark(entry.coordinates, {
                hintContent: entry.title,
                balloonContent: entry.title
            });
            myMap.geoObjects.add(myPlacemark);
        });
    }
}